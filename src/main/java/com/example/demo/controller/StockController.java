package com.example.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.entities.Stock;
import com.example.demo.service.StockService;

@RestController
@RequestMapping("api/stocks")
public class StockController {

	@Autowired
	StockService service;

	@GetMapping(value = "/")
	public List<Stock> getAllStock() {
		return service.getAllStock();
	}

	@GetMapping(value = "/{id}")
	public Stock getStockById(int id) {
		return service.getStockById(id);
	}

	@DeleteMapping(value = "/{id}")
	public int deleteStock(int id) {
		return service.deleteStock(id);
	}

	@PostMapping(value = "/")
	public Stock addStock(Stock stock) {
		return service.addStock(stock);
	}

	@PutMapping(value = "/")
	public Stock editStock(Stock stock) {
		return service.editStock(stock);
	}

}
